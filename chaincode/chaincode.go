package main

import (
	"fmt"
    "strings"
    "encoding/json"
    "time"

    "bitbucket.org/wouterraateland/bv/chaincode/transaction"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

const (
    ERROR_METHOD_NOT_SUPPORTED int = iota
    ERROR_ARGUMENT_NUMBER
    ERROR_CERTIFICATE_INVALID
)

type ChaincodeError struct {
    code int
    message string
}

func (e *ChaincodeError) Error() string {
    return fmt.Sprintf("%d - %s", e.code, e.message)
}

type InvoiceChaincode struct {
}

var logger = shim.NewLogger("BVChaincode")

// =========================================================================
// Main
// =========================================================================
func main() {
	err := shim.Start(new(InvoiceChaincode))
	if err != nil {
		fmt.Printf("Error starting chaincode: %s", err)
	}
}

// Init resets all the things
func (t *InvoiceChaincode) Init(stub shim.ChaincodeStubInterface,
    function string, args []string) ([]byte, error) {

    err := expectArgs(args, 0)
    if err != nil { return nil, err }

    return nil, nil
}

// Invoke is our entry point to invoke a chaincode function
func (t *InvoiceChaincode) Invoke(stub shim.ChaincodeStubInterface,
    function string, args []string) ([]byte, error) {

    caller, err := t.get_username(stub)
    if err != nil { return nil, err }
    ts, err := t.get_timestamp(stub)
    if err != nil { return nil, err }

    logger.Debug("Function: ", function)
    logger.Debug("Caller: ", caller)
    logger.Debug("Timestamp: ", t)

    parts := strings.Split(function, ".")

    switch parts[0] {
    case "singleInvoice":
        
        switch parts[1] {
        case "notarize":
            err := expectArgs(args, 2)
            if err != nil { return nil, err }
            return transaction.NotarizeSingle(stub, caller, ts, args[0], args[1])

        case "revoke":
            err := expectArgs(args, 2)
            if err != nil { return nil, err }
            return transaction.RevokeSingle(stub, caller, ts, args[0], args[1])
        }

    case "period":
        switch parts[1] {
        case "notarize":
            err := expectArgs(args, 2)
            if err != nil { return nil, err }

            endTime, err := time.Parse(time.RFC3339, args[1])
            if err != nil { return nil, err }

            return transaction.NotarizePeriod(stub, caller, ts, args[0], endTime)

        case "revoke":
            err := expectArgs(args, 1)
            if err != nil { return nil, err }
            return transaction.RevokePeriod(stub, caller, ts, args[0])
        }

    case "validator":
        // TODO: Implement validator
        return nil, nil
    }

    return nil, &ChaincodeError{
        code: ERROR_METHOD_NOT_SUPPORTED,
        message: fmt.Sprintf("Method %s not supported", function),
    }
}

// Query is our entry point for queries
func (t *InvoiceChaincode) Query(stub shim.ChaincodeStubInterface,
    function string, args []string) ([]byte, error) {

    caller, err := t.get_username(stub)
    if err != nil { return nil, err }
    ts, err := t.get_timestamp(stub)
    if err != nil { return nil, err }

    logger.Debug("Function: ", function)
    logger.Debug("Caller: ", caller)
    logger.Debug("Timestamp: ", t)

    switch function {
    case "singleInvoice.verify":
        err := expectArgs(args, 2)
        if err != nil { return nil, err }
        verified, message, err := transaction.VerifySingle(stub, caller, ts, args[0], args[1])
        return verificationMessage(verified, message, err)

    case "period.verify":
        err := expectArgs(args, 1)
        if err != nil { return nil, err }
        verified, message, err := transaction.VerifyPeriod(stub, caller, ts, args[0])
        return verificationMessage(verified, message, err)

    case "read":
        err := expectArgs(args, 1)
        if err != nil { return nil, err }
        val, err := stub.GetState(args[0])
        if err != nil { return nil, err }
        return val, nil
    }

    return nil, &ChaincodeError{
        code: ERROR_METHOD_NOT_SUPPORTED,
        message: fmt.Sprintf("Method %s not supported", function),
    }
}

func (t *InvoiceChaincode) get_username(stub shim.ChaincodeStubInterface) (string, error) {

    // username, err := stub.ReadCertAttribute("username")

    // if err != nil {
    //     return "", &ChaincodeError{
    //         code: ERROR_CERTIFICATE_INVALID,
    //         message: err.Error(),
    //     }
    // }
    
    // return string(username), nil
    return "", nil
}

func (t *InvoiceChaincode) get_timestamp(stub shim.ChaincodeStubInterface) (time.Time, error) {
    ts, err := stub.GetTxTimestamp()
    if err != nil { return time.Time{}, err }
    
    return time.Unix(int64(ts.Seconds), int64(ts.Nanos)), nil
}

func verificationMessage(verified bool, message string, err error) ([]byte, error) {
    if err != nil { return nil, err }

    verification := &transaction.Verification{
        Verified: verified,
        Message: message,
    }

    bytes, err := json.Marshal(verification)
    if err != nil { return nil, err }
    return bytes, nil
}

func expectArgs(args []string, n int) error {
    if len(args) != n {
        return &ChaincodeError{
            code: ERROR_ARGUMENT_NUMBER,
            message: fmt.Sprintf("Expected %d arguments, got %d", n, len(args)),
        }
    }

    return nil
}