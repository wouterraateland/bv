package transaction

import (
	"fmt"
)

const (
	ERROR_FORMAT int = iota
	ERROR_VERSION
	ERROR_TYPE_NOT_SUPPORTED
	ERROR_PAYLOAD
	ERROR_TYPE_VALIDATION
	ERROR_DUPLICATE_AGREEMENT
	ERROR_USER_MISMATCH
)

type TransactionError struct {
	code int
	message string
}

func (e *TransactionError) Error() string {
	return fmt.Sprintf("%d - %s", e.code, e.message)
}

type Verification struct {
	Verified bool `json:"verified"`
	Message string `json:"message"`
}