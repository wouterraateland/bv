package transaction

import (
	"fmt"
	"time"
	"encoding/json"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

type SingleInvoice struct {
	Owner string `json:"owner"`
	Revoked bool `json:"revoked"`
	Ts time.Time `json:"created_at"`
}

func FindSingle(stub shim.ChaincodeStubInterface,
	sellerId string, invoiceId string) (*SingleInvoice, error) {

	bytes, err := stub.GetState(getIdentifier(sellerId, invoiceId))
	if err != nil { return nil, err }
	if bytes == nil { return nil, nil }

	var agreement SingleInvoice

	if err := json.Unmarshal(bytes, &agreement); err != nil {
		return nil, err
	}

	return &agreement, nil
}

func VerifySingle(stub shim.ChaincodeStubInterface, caller string, ts time.Time,
	sellerId string, invoiceId string) (bool, string, error) {

	singleInvoice, err := FindSingle(stub, sellerId, invoiceId)
	if err != nil { return false, "", err }

	if singleInvoice != (&SingleInvoice{}) && singleInvoice != nil { // Single-Invoice agreement is found
		if singleInvoice.Revoked {
			return true, "Revoked or exempted invoice agreement", nil
		} else if singleInvoice.Owner == caller {
			return false, "Single-invoice agreement registered by youself", nil
		} else {
			return false, "Single-invoice agreement already exists", nil
		}
	}

	periodAgreement, err := FindPeriod(stub, sellerId)
	if err != nil { return false, "", err }

	if periodAgreement != nil && periodAgreement != (&Period{}) {
		if periodAgreement.Revoked {
			return true, "Revoked or exempted period agreement", nil
		} else if ts.After(periodAgreement.EndTime) {
			return true, "Expired period agreement", nil
		} else if periodAgreement.Owner == caller {
			return false, "Invoice in long-term agreement registered by youself", nil
		} else {
			return false, "Invoice in existing long-term agreement", nil
		}
	}

	return true, "No previous agreements", nil
}

func NotarizeSingle(stub shim.ChaincodeStubInterface, caller string, ts time.Time,
	sellerId string, invoiceId string) ([]byte, error) {

	free, message, err := VerifySingle(stub, caller, ts, sellerId, invoiceId)
	if err != nil { return nil, err }

	if !free {
		return nil, &TransactionError{
			code: ERROR_DUPLICATE_AGREEMENT,
			message: message,
		}
	}

	return saveSingle(stub, caller, ts, sellerId, invoiceId, false)
}

func RevokeSingle(stub shim.ChaincodeStubInterface, caller string, ts time.Time,
	sellerId string, invoiceId string) ([]byte, error) {

	free, message, err := VerifySingle(stub, caller, ts, sellerId, invoiceId)
	if err != nil { return nil, err }

	if free {
		return nil, &TransactionError{
			code: ERROR_DUPLICATE_AGREEMENT,
			message: "Agreement not yet notarized",
		}
	}

	if message != "Single-invoice agreement registered by youself" {
		return nil, &TransactionError{
			code: ERROR_USER_MISMATCH,
			message: "Agreement notarized by different user",
		}
	}

	return saveSingle(stub, caller, ts, sellerId, invoiceId, true)
}

func saveSingle(stub shim.ChaincodeStubInterface, caller string, ts time.Time,
	sellerId string, invoiceId string, revoked bool) ([]byte, error) {
	key := getIdentifier(sellerId, invoiceId)

	fmt.Println(key)

	agreement := &SingleInvoice{
		Owner: caller,
		Revoked: revoked,
		Ts: ts,
	}

	bytes, err := json.Marshal(agreement)
	if err != nil { return nil, err }

	fmt.Println(string(bytes))

	err = stub.PutState(key, bytes)
	if err != nil { return nil, err }

	return nil, nil
}

func getIdentifier(sellerId string, invoiceId string) string {
	return sellerId + ";" + invoiceId
}