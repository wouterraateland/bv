package transaction

import (
	"time"
	"encoding/json"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

type Period struct {
	Owner string `json:"owner"`
	Revoked bool `json:"revoked"`
	Ts time.Time `json:"created_at"`
	EndTime time.Time `json:"ends_at"`
}

func FindPeriod(stub shim.ChaincodeStubInterface,
	sellerId string) (*Period, error) {

	bytes, err := stub.GetState(sellerId)
	if err != nil { return nil, err }
	if bytes == nil { return nil, nil }

	var agreement Period

	if err := json.Unmarshal(bytes, &agreement); err != nil {
		return nil, err
	}

	return &agreement, nil
}

func VerifyPeriod(stub shim.ChaincodeStubInterface, caller string, ts time.Time,
	sellerId string) (bool, string, error) {

	period, err := FindPeriod(stub, sellerId)
	if err != nil { return false, "", err }

	if period != (&Period{}) && period != nil { // Period agreement is found
		if period.Revoked {
			return true, "Revoked or exempted period agreement", nil
		} else {
			if period.EndTime.Before(ts) {
				return true, "Expired period agreement", nil
			} else if period.Owner == caller {
				return false, "Period agreement registered by youself", nil
			} else {
				return false, "Period agreement already exists", nil
			}
		}
	}

	return true, "No previous agreements", nil
}

func NotarizePeriod(stub shim.ChaincodeStubInterface, caller string, ts time.Time,
	sellerId string, endTime time.Time) ([]byte, error) {

	free, message, err := VerifyPeriod(stub, caller, ts, sellerId)
	if err != nil { return nil, err }

	if !free {
		return nil, &TransactionError{
			code: ERROR_DUPLICATE_AGREEMENT,
			message: message,
		}
	}

	return savePeriod(stub, caller, ts, sellerId, endTime, false)
}

func RevokePeriod(stub shim.ChaincodeStubInterface, caller string, ts time.Time,
	sellerId string) ([]byte, error) {

	free, message, err := VerifyPeriod(stub, caller, ts, sellerId)
	if err != nil { return nil, err }

	if free {
		return nil, &TransactionError{
			code: ERROR_DUPLICATE_AGREEMENT,
			message: "Agreement not yet notarized",
		}
	}

	if message != "Period agreement registered by youself" {
		return nil, &TransactionError{
			code: ERROR_USER_MISMATCH,
			message: "Agreement notarized by different user",
		}
	}

	oldPeriod, err := FindPeriod(stub, sellerId)
	if err != nil { return nil, err }

	return savePeriod(stub, caller, ts, sellerId, oldPeriod.EndTime, true)
}

func savePeriod(stub shim.ChaincodeStubInterface, caller string, ts time.Time,
	sellerId string, endTime time.Time, revoked bool) ([]byte, error) {
	
	agreement := &Period{
		Owner: caller,
		Revoked: revoked,
		Ts: ts,
		EndTime: endTime,
	}

	bytes, err := json.Marshal(agreement)
	if err != nil { return nil, err }

	err = stub.PutState(sellerId, bytes)
	if err != nil { return nil, err }

	return nil, nil
}