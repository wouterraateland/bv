# DecReg - The blockchain solution against double spending

## Prerequisites

- Have node.js installed
- Have Go installed

## Installation

- Clone this repository:

```
cd $GOPATH/
mkdir $GOPATH/src/bitbucket.org/
cd $GOPATH/src/bitbucket.org/
git clone https://wouterraateland@bitbucket.org/wouterraateland/bv.git
```

## Testing the setup

```
npm test
```

## Running the node

```
npm start
```

## Working with the API

The API is designed to be as easy as possible to integrate with your current system.

Before you can interact with the API you have to authenticate yourself. You can do this using the certificate you got from the certificate authority (CA).

When you have authenticated yourself, communicating with the API is as simple as posting request to ```localhost:3000/{route}```

### Single invoices

- View all your notarised invoices  
  Route:  GET invoice/all/  
  Params: None

- View the details of a single notarised invoice  
  Route:  GET invoice/single/  
  Params: invoice id

- Verify that an invoice is notarised  
  Route:  GET invoice/single/  
  Params: invoice number, seller CoC number

- Notarise a new invoice  
  Route:  POST invoice/notarise/
  Params: invoice number, seller CoC number

- Revoke a notarised invoice  
  Route:  POST invoice/revoke/  
  Params: invoice id

### Long-term agreements

- View all your notarised long-term agreements  
  Route:  GET longterm/all/  
  Params: None

- View the details of a single notarised long-term agreement  
  Route:  GET longterm/single/  
  Params: agreement id

- Notarise a new long-term agreement  
  Route:  POST longterm/notarise/  
  Params: start date, end date, seller CoC number

- Revoke a notarised long-term agreement  
  Route:  POST longterm/revoke/  
  Params: agreement id