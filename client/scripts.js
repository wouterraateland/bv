const MS_PER_YEAR = 365*24*60*60*1000

const ajax = (options, next) => {
	const xhr = new XMLHttpRequest()
	
	xhr.onreadystatechange = function () {
		if (this.readyState == 4) {
			if (this.status == 200) {
				next(this.responseText, null)
			} else {
				next(this.responseText, this.status)
			}
		}
	};

	xhr.open(options.method, options.url)
	xhr.setRequestHeader(
		"Content-Type", "application/json");

	if (options.method === 'post' && options.data) {
		xhr.send(JSON.stringify(options.data))
	} else {
		xhr.send()
	}
}

const getDatetime = ts =>
	(new Date(ts.seconds*1000 + ts.nanos/1000)).toLocaleString()

const generateLogEntry = transaction =>
	`<li>
		<span class="timestamp">${transaction.datetime}</span>
		<span class="certificate">${transaction.certificate}</span>
		<span class="payload">${transaction.payload}</span>
	</li>`

const logBlocks = json => {
    json = JSON.parse(json)

    let blocks = []

    for (key in json) { blocks.push(json[key]) }

    const transactions = blocks
		.map(block => block.transactions)
		.reduce((acc, el) => (el == undefined) ? acc : acc.concat(el), [])
		.map(transaction => ({
			datetime: getDatetime(transaction.timestamp),
			certificate: transaction.cert.substr(0, 20),
			payload: transaction.payload.substr(0, 20)
		}))
		.reduce((acc, tx) => acc + generateLogEntry(tx), "")

    return transactions
}

const actionForm = document.querySelector('#actionForm')
const invoiceIdLabel = document.querySelector('#invoiceIdLabel')
const queryMessage = document.querySelector('#queryMessage')
const transactionLog = document.querySelector('#transactionLog')
const notarizeButton = document.querySelector('#notarize')

const setTxType = () => {
	const period = actionForm['transactionType'].checked
	actionForm.classList.toggle('period', period)
	actionForm['invoiceId'].required = !period
}

const submit = event => {
	event.preventDefault()
	actionForm.classList.add('loading')

	const type = actionForm['transactionType'].checked ?
		'period' : 'singleInvoice'
	const data = actionForm['transactionType'].checked ? {
			sellerId: actionForm['sellerId'].value
		} : {
			invoiceId: actionForm['invoiceId'].value,
			sellerId: actionForm['sellerId'].value
		};

	ajax({
		method: 'post',
		url: `/api/${type}/verify`,
		data
	}, (res, err) => {
		queryMessage.classList.toggle('error', err)

		console.log(res.replace(/\\/g, '').replace('"{', '{').replace('}"', '}'))
		const data = JSON.parse(res.replace(/\\/g, '').replace('"{', '{').replace('}"', '}'))

		queryMessage.innerText = data.message.message
		notarizeButton.classList.toggle('active', data.message.verified)
		notarizeButton.dataset.type = type
		notarizeButton.dataset.sellerId = actionForm['sellerId'].value
		notarizeButton.dataset.invoiceId = actionForm['invoiceId'].value

		actionForm.classList.remove('loading')
	})
}

const notarize = event => {
	const single = notarizeButton.dataset.type == 'singleInvoice'
	const data = single ? {
			sellerId: notarizeButton.dataset.sellerId,
			invoiceId: notarizeButton.dataset.invoiceId
		} : {
			sellerId: notarizeButton.dataset.sellerId,
			endTime: new Date(Date.now() + MS_PER_YEAR)
		}

	console.log(data)

	ajax({
		method: 'post',
		url: `/api/${notarizeButton.dataset.type}/notarize`,
		data
	}, (res, err) => {
		queryMessage.innerHTML = err ? 'Error notarizing the agreement' : 'Agreement notarized succesfully!'
		queryMessage.classList.toggle('error', err)

		notarizeButton.classList.toggle('active', err)
	})
}

const refresh = () => ajax({
	method: 'get',
	url: '/api/log'
}, (res, err) => {
	transactionLog.innerHTML = logBlocks(res)
})

actionForm['transactionType'].addEventListener('change', setTxType)
actionForm.addEventListener('submit', submit)
notarizeButton.addEventListener('click', notarize)

refresh()
setInterval(refresh, 15000)