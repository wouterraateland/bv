const chaincode = require('./server/chaincode')
const chain = require('./server/util/chain')
const api = require('./server/api/')

const express = require('express')
const bodyParser = require('body-parser')
const app = express()

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const credentials = {
	enrollId: 'user_type1_1',
	enrollSecret: 'ff582e56e6'
}

const enroll = next =>
	chaincode.isEnrolled(credentials.enrollId, (enrolled, err) => {
		if (err) { return console.error(err) }

		if (enrolled) {
			console.log('User already logged in')
			return next()
		}
		
		chaincode.enroll(credentials, (res, err) => {
			if (err) { return console.error(err) }

			console.log('User enrolled')
			return next()
		})

	})

const deploy = next =>
	chaincode.isDeployed((deployed, err) => {
		if (err) { return console.error(err) }

		if (deployed) {
			console.log('Chaincode already deployed')
			return next()
		}

		console.log('Chaincode not yet deployed. Deploying...')

		chaincode.deploy('init', [], (res, err) => {
			if (err) { return console.error(err) }

			console.log('Chaincode deployed')
			return next()
		})
	})

const initApi = next => {
	app.use('/api', api.getRouter())
	console.log('API initialised')
	next()
}

const initApp = next => {
	app.get('/', (req, res) => {
		res.sendFile(__dirname + '/client/index.html')
	})

	app.get('/deploy', (req, res) => {
		chaincode.deploy('init', [], (response, err) => {
			if (err) {
				return res.send(`Error deploying chaincode: ${err}`)
			}
			res.send('Chaincode deployed')
		})
	})

	app.use(express.static('./client'))

	console.log('App initialised')

	next()
}

chain([
	enroll,
	deploy,
	initApi,
	initApp,
	next => app.listen(3000, () => {
		console.log('Blockchain Verpandingsregister listening on port 3000!')
		next()
	})
])