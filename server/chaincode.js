const request = require('request')
const fs = require('fs')

let chaincodeData = require('./chaincode.json')

let secureContext = null

const isEnrolled = (enrollId, next) =>
	request.get(
		`${chaincodeData.uri}/registrar/${enrollId}`,
		{ json: {} },
		(err, res, body) => {
			if (err || res.statusCode !== 200) {
				return next(false, null)
			} else {
				secureContext = enrollId
				return next(true, null)
			}
		}
	)

const enroll = (credentials, next) =>
	request.post(
		`${chaincodeData.uri}/registrar`,
		{ json: credentials },
		(err, res, body) => {
			if (err || res.statusCode !== 200) {
				return next(null, body.Error)
			}
			secureContext = credentials.enrollId
			next(body, null)
		}
	)

const block = (n, next) => {
	request.get(
		`${chaincodeData.uri}/chain/blocks/${n}`,
		{ json: {} },
		(err, res, body) => {
			if (err || res.statusCode !== 200) {
				return next(null, err)
			}

			next(body, null)
		}
	)
}

const blockchain = next => {
	request.get(
		`${chaincodeData.uri}/chain`,
		{ json: {} },
		(err, res, body) => {
			if (err || res.statusCode !== 200) {
				return next(null, err)
			}

			next(body, null)
		}
	)
}

const chaincode = (method, fun, args, next) => {
	if (!secureContext) {
		return next(null, new Error('No secure context defined. Enroll first.'))
	}

	if (!chaincodeData.name && method !== 'deploy') {
		return next(null, new Error('No chaincode deployed yet.'))
	}

	console.log(fun)
	console.log(args)

	request.post(
		`${chaincodeData.uri}/chaincode`,
		{ json: {
			"jsonrpc": "2.0",
			"method": method,
			"params": {
				"type": 1,
				"chaincodeID": {
					path: chaincodeData.path,
					name: chaincodeData.name
				},
				"ctorMsg": {
					"function": fun,
					"args": args
				},
				"secureContext": secureContext
			},
			"id": 1
		}},
		(err, res, body) => {
			if (err) {
				console.log(body.error.message)
			} else {
				console.log(body.result)
			}
			
			if (err || body.error || res.statusCode !== 200) {
				return next(null, body.error)
			}

			// Set chaincode id
			if (method == 'deploy') {
				chaincodeData = Object.assign(chaincodeData, {
					name: body.result.message
				})

				const json = JSON.stringify(chaincodeData)

				fs.writeFile('server/chaincode.json', json, 'utf8', () => {
					next(body.result, null)
				})
			} else {
				next(body.result, null)
			}
		}
	)
}

const isDeployed = next => next(!!chaincodeData.name, null)

const deploy = (fun, args, next) =>
	chaincode('deploy', fun, args, next)
const invoke = (fun, args, next) =>
	chaincode('invoke', fun, args, next)
const query = (fun, args, next) =>
	chaincode('query', fun, args, next)

module.exports = { isEnrolled, enroll, block, blockchain, isDeployed, deploy, invoke, query }