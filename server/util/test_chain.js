const chain = require('./chain')

chain([
	next => {
		console.log('1')
		next()
	},
	next => {
		console.log('2')
		next()
	},
	() => {
		console.log('3')
	},
	next => {
		console.log('4')
		next()
	},
])
// Should print 1,2,3