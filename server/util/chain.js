/*
Executes functions after each other.
*/

const next = fns => () => {
	if (typeof fns[0] == 'function') {
		fns[0](next(fns.slice(1)))
	}
}

const chain = fns =>
	next(fns)()

module.exports = chain
