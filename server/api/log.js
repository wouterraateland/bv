const chaincode = require('../chaincode')

const express = require('express')

let blocks = {}

const pass = response => (result, err) => {
	if (err) {
		return response.status(500).send(err)
	}

	response.send(result)
}

const getBlocks = next => {
	chaincode.blockchain((res, err) => {
		if (err) { return next(null, err) }

		let blocksCount = 0
		let blocksLeft = 0
		let sent = false

		for (let h = 0; h < res.height; h++) {
			if (blocks[h] === undefined) {
				blocksCount++
				blocksLeft++
				
				chaincode.block(h, (res, err) => {
					blocksLeft--

					if (!err) {
						blocks = Object.assign(blocks, {[h]: res})
					}

					if (blocksLeft <= 0 && !sent) {
						sent = true
						return next(blocks, null)
					}
				})
			}
		}

		if (blocksCount == 0) {
			return next(blocks, null)
		}
	})
}

const getRouter = () => {
	const router = express.Router()

	router.get('/', (req, res) => {
		getBlocks(pass(res))
	})

	return router
}

module.exports = {
	getRouter
}