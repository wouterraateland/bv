const express = require('express')

const singleInvoice = require('./singleInvoice')
const period = require('./period')
const log = require('./log')

const getRouter = () => {
	const router = express.Router()
	router.use('/singleInvoice', singleInvoice.getRouter())
	router.use('/period', period.getRouter())
	router.use('/log', log.getRouter())

	return router
}

module.exports = {
	getRouter
}