const chaincode = require('../chaincode')

const express = require('express')

const pass = response => (result, err) => {
	if (err) {
		return response.status(500).send(err)
	}

	response.send(result)
}

const getRouter = () => {
	const router = express.Router()

	router.post('/verify', (req, res) => {
		const args = [req.body.sellerId]
		chaincode.query('period.verify', args, pass(res))
	})

	router.post('/notarize', (req, res) => {
		const args = [req.body.sellerId, req.body.endTime]
		chaincode.invoke('period.notarize', args, pass(res))
	})

	router.post('/revoke', (req, res) => {
		const args = [req.body.sellerId]
		chaincode.invoke('period.revoke', args, pass(res))
	})

	return router
}

module.exports = {
	getRouter
}